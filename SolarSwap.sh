#!/usr/bin/env bash
# The shortcuts called in the darwin set functions are for changing the iterm2 theme.
# These must be setup under iterm2->Preferences->Keys->Key Bindings->+ and they must
# use the appropiate binding for the mode.  I.E. dark is set to Ctrl+Opt+Shift+d.
# These shortcuts should be set to "Change Profile to ..." the configured profile with
# the desired colorscheme.

THEME=catppuccin
AIRLINE_THEME=catppuccin
DEFAULT_MODE="dark"
DARK_VARIANT="mocha"
LIGHT_VARIANT="latte"
SOLARSWAP_CONFIG_DIR=$HOME/.solarswap
SOLARSWAP_MODE_FILE=$SOLARSWAP_CONFIG_DIR/solarswap_mode.sh
VIMRC_BACKGROUND_FILE=$SOLARSWAP_CONFIG_DIR/vimrc_background.vim
VIMRC_BACKGROUND_LUA_FILE=$SOLARSWAP_CONFIG_DIR/vimrc_background.lua
TMUX_CURRENT_THEME_FILE=$SOLARSWAP_CONFIG_DIR/tmux_current_theme.tmux
KITTY_CURRENT_THEME_FILE=$SOLARSWAP_CONFIG_DIR/kitty_current_theme.conf
TMUX_CONF_FILE=$HOME/.tmux.conf

function darwin_set_mode() {
	local background=${1:-DEFAULT_MODE}
	case ${background} in
	dark)
		local sys_mode='true'
		;;
	light)
		local sys_mode='not dark mode'
		;;
	esac
	osascript -e "tell app \"System Events\" to tell appearance preferences to set dark mode to ${sys_mode}"
}

function linux_set_mode() {
	#  https://wiki.archlinux.org/title/Dark_mode_switching
	# TODO: Add check for desktop environment and handle response #
	local background=${1:-DEFAULT_MODE}
	gsettings set org.gnome.desktop.interface color-scheme "prefer-${background}"
}

function setup_solarswap_mode_file() {
	local background=${1:-DEFAULT_MODE}
	cat <<-EOF >"$SOLARSWAP_MODE_FILE"
		SOLARSWAP_MODE=${background}
	EOF
}

function setup_vimrc_background_file() {
	local background=${1:-DEFAULT_MODE}
	cat <<-EOF >"$VIMRC_BACKGROUND_FILE"
		set background=${background}
		colorscheme ${THEME}
		let g:airline_theme='${AIRLINE_THEME}'
	EOF
}

function setup_vimrc_background_lua_file() {
	local background=${1:-DEFAULT_MODE}
	cat <<-EOF >"$VIMRC_BACKGROUND_LUA_FILE"
		vim.cmd.colorscheme("${THEME}")
		vim.opt.background = "${background}"
	EOF
}

function setup_tmux_current_theme_file() {
	local background=${1:-DEFAULT_MODE}
	cat <<-EOF >"$TMUX_CURRENT_THEME_FILE"
		    source-file ~/.tmux/themes/${THEME}-${background}.tmux
	EOF
}

function setup_kitty_current_theme_file() {
	local background=${1:-DEFAULT_MODE}
	case ${background} in
		"dark")
			theme=$DARK_VARIANT ;;
		"light")
			theme=$LIGHT_VARIANT ;;
	esac
	cat <<-EOF >"$KITTY_CURRENT_THEME_FILE"
		include ~/.config/kitty/${theme}.conf
	EOF
}

function get_current_mode() {
	if [[ -e "${SOLARSWAP_MODE_FILE}" ]]; then
		# shellcheck disable=1090
		source "${SOLARSWAP_MODE_FILE}"
		echo "$SOLARSWAP_MODE"
		return 0
	fi
	echo "$DEFAULT_MODE"
	return 0
}

function main() {
	[[ ! -d "$SOLARSWAP_CONFIG_DIR" ]] && mkdir "$SOLARSWAP_CONFIG_DIR"
	case $(get_current_mode) in
	dark)
		background=light
		;;
	light)
		background=dark
		;;
	esac
	echo "Setting ${THEME} ${background}..."
	setup_solarswap_mode_file "${background}"
	setup_vimrc_background_file "${background}"
	setup_vimrc_background_lua_file "${background}"
	setup_tmux_current_theme_file "${background}"
	setup_kitty_current_theme_file "${background}"
	sh -c "tmux source-file $TMUX_CONF_FILE"
	case $(uname -s) in
	Darwin)
		darwin_set_mode "${background}"
		;;
	Linux)
		linux_set_mode "${background}"
		;;
	esac
	return 0
}

main && exit 0
exit 1
